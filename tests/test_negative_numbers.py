import unittest

from src.app import sum_of_numbers


class NegativeNumbers(unittest.TestCase):

    def test_negative_numbers(self):
        result = sum_of_numbers(-2, -1)
        self.assertEqual(result, -3, "{} + {} !={}".format(-2, -1, -3))
        if result is -3:
            print(self, 'test passed')
        else:
            print(self, 'Error!!!')

    def test_negative_numbers_2(self):
        result = sum_of_numbers(-2, -1)
        self.assertTrue(result !=-1)
        print(self, 'test passed')

    def test_negative_numbers_3(self):
        result = sum_of_numbers(-2, -1)
        self.assertNotEqual(result,-1, "{} + {} !={}".format(-1, -1, -1))
        print(self, 'test passed')

    def test_negative_numbers_4(self):
        a = -1
        b = -2
        expected_result = -3
        result = sum_of_numbers(a, b)
        self.assertEqual(result, expected_result, "{} + {} ={}".format(a, b, expected_result))
        print(self,'test passed')

    def test_negative_numbers_5(self):
        a = -2
        b = -2
        expected_result = -4
        result = sum_of_numbers(a, b)
        self.assertFalse(expected_result is not result,
                         "Actual result: {}, expected result: {}".format(
                             result, expected_result))
