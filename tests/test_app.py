import unittest
from src.app import sum_of_numbers

form = "{} + {} is not {} "


class TestSumNatural(unittest.TestCase):

    def test_sum_natural_number(self):
        a = 5
        b = 6
        c = 11
        d = 3.14
        result = sum_of_numbers(a, b)
        self.assertEqual(result, c, form.format(a, b, c))
        self.assertTrue(result != d)
        self.assertNotEqual(result, d, form.format(a, b, d))


class TestSumZeros(unittest.TestCase):
    def test_sum_zeros(self):
        a = 0
        b = 0
        c = 0
        result = sum_of_numbers(a, b)
        self.assertEqual(result, c, form.format(a, b, c))


class TestNegativeNumbers(unittest.TestCase):

    def test_sum_negative_numbers(self):
        a = -1
        b = -1
        c = -2
        d = 0
        result = sum_of_numbers(a, b)
        self.assertEqual(result, c, form.format(a, b, c))
        self.assertNotEqual(result, d, form.format(a, b, d))

    def test_sum_first_negative_number(self):
        a = -1
        b = 1
        c = 0
        d = -2
        result = sum_of_numbers(a, b)
        self.assertEqual(result, c, form.format(a, b, c))
        self.assertNotEqual(result, d, form.format(a, b, d))

    def test_sum_first_positive_number(self):
        a = 1
        b = -1
        c = 0
        d = 2
        result = sum_of_numbers(a, b)
        self.assertEqual(result, c, form.format(a, b, c))
        self.assertNotEqual(result, d, form.format(a, b, d))


class TestLogin(unittest.TestCase):

    def test_log_in_as_admin(self):
        result = sum_of_numbers(100, 10)
        self.assertEqual(result, 110, "{} + {} != {}".format(100, 10, 110))
