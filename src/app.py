
class SumOfNumbers(object):

    def __init__(self):
        self.__auth = None
        self.__creds = {
            "username": "admin",
            "password": "welcome123"
        }

    def login(self, username, password):
        if (self.__creds.get("username") == username and
                self.__creds.get("password") == password):
            self.__auth = True

    def sum_of_numbers(self, a, b):
        if self.__auth:
            a = int(a)
            b = int(b)
            if a < 0:
                print("{} is negative")
            return a + b
        raise Exception("You need to log in in order to use the app!")
